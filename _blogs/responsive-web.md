---
layout: blogs
permalink: /blogs/responsive-web/
title: Responsive
img_path: /static/images/pro.JPG
description: Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolorum libero veniam accusamus deleniti, assumenda cumque?
tag1: Html
tag2: css
---


# Installation

It isn't very strict but, for your own ease, it is recommended to download and install a code editor like Visual Studio Code, or Sublime or any code editor of your choice. The workshop code can be done even in notepad, but for easier understanding and lesser errors, a code editor is very highly recommended. 

Please make sure that you have Python and pip installed in your system.

### For Windows

Visit [https://www.python.org/](https://www.python.org/) , download 'Windows x86-64 executable installer' and open the installer.

**"Important: Do tick the box labelled _"Add Python 3.x to PATH "_ !!**

Open command prompt and enter `python`. If it returns >>> prompt, python installation was successful.

### For Linux

Usually, python will be pre-installed. Open terminal and type `python3` then enter to check. If >>> prompt is seen, python is installed in the system.

----------------------------------------------------------
## Next, install Django using pip

### For Windows

`pip install django`

### For Linux

`pip3 install django`

_Rest we 'll learn during the workshop. Do set up this much for a quicker and easier workshop. Hava a nice day :) _
